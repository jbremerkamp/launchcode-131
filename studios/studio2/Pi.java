package studio2;

public class Pi {

	public static void main(String[] args) 
	{
		//set total number of darts
		int numDarts = 10000000;
		int circleCount = 0;
		//for each dart
		for(int i = 0; i < numDarts; i++)
		{
			//random x coordinate
			double x = Math.random();
			//Math.random * 10 - 5
			//random y coordinate
			double y = Math.random();
		
			//distance = sqrt((randomx - midx)^2 + (randomy - midy)^2)
			double xpart = Math.pow(x - .5, 2);
			double ypart = Math.pow(y - .5, 2);
			double distance = Math.sqrt(xpart + ypart);
			
			
			//if dart is inside circle
			if(distance <= .5)
			{
				//increase circle count
				//int circleCount = 0; put this in front of the loop
				circleCount++;
			}
		}
				
		//circlecount / number of darts * 4
		System.out.println(circleCount / (double)numDarts * 4);
		
		

	}

}
