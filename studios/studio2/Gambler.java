package studio2;
import cse131.ArgsProcessor;

public class Gambler {

	public static void main(String[] args) {
ArgsProcessor ap = new ArgsProcessor(args);
        
        double startAmount = ap.nextDouble("How much money do you have?");
        double winChance = ap.nextDouble("Chances of winning?");
        double winAmount = ap.nextDouble("How much money do you want to make?");
        double totalPlays = ap.nextInt("How many times would you like to play?");
        
        System.out.println("Starting Amount: $" + startAmount);
        System.out.println("Probability you will win: " + winChance);
        System.out.println("Winning Amount: $" + winAmount);
        System.out.println("Number of plays: " + totalPlays);
        
        double lossChance = 1 - winChance;
        double ruin = 0.0;
        double loss = 0.0;
                
        for (int i = 1; i <= totalPlays; i++){
            double currentAmount = startAmount;
            int rounds = 1;
            
            while (currentAmount > 0 && currentAmount != winAmount){
                
                if (Math.random() < winChance){
                    currentAmount = currentAmount + 1;
                    rounds = rounds +1;
                }
                else {
                    currentAmount = currentAmount - 1;
                    rounds = rounds +1;
                }
            }
            if (currentAmount == 0){
                System.out.println("Simulation " + i + ": " + rounds + " rounds\t LOSS");
                loss = loss + 1.0;
            } else {
                System.out.println("Simulation " + i + ": " + rounds + " rounds\t WIN");
            }
        }
            System.out.print("Losses: " + loss +"\t");
            System.out.println("Simulation: " + totalPlays);
            
                
            if (lossChance != winChance){
                ruin = (Math.pow((lossChance / winChance), startAmount) - Math.pow((lossChance/winChance), winAmount)) / (1 - Math.pow((lossChance/winChance), winAmount));
            }
            if (lossChance == winChance){
                ruin = 1 - (startAmount / winAmount);
            }
            System.out.println("Actual Ruin Rate: " + (loss / totalPlays));
            System.out.println(ruin);
    }
}
