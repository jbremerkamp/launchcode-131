import cse131.ArgsProcessor;
public class Grid {

	public static void main(String[] args) {
		
		ArgsProcessor ap =new ArgsProcessor(args);
		int x = ap.nextInt("How many rows?");
		int y = ap.nextInt("How many columns?");
		
		for (int i = 0; i < 4; i++) {
			
			for (int j = 0; j < 6;j++){
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
