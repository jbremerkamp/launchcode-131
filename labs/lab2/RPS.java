package lab2;

import cse131.ArgsProcessor;

public class RPS {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		
		int rounds = ap.nextInt("How many rounds do you want to play?");
		
		System.out.println("Total rounds to play =" + rounds);
		
		String r = "Rock";
		String p = "Paper";
		String s = "Scissors";
		String player1 = null;
		String player2 = "S";
		double p2Win = 0.0;
		double p1Win = 0.0;
		double tie = 0.0;
		
		
		
		
		
		
		
	    for(int i = 1; i <= rounds; i++)
	    {
	    	
	    	double randomPlay = Math.random();
	    	if(randomPlay < 0.34) {
	    		player1 = r;
	    		System.out.println("Rock");
	    		
	    	} else if(randomPlay <= 0.67) {
	    		player1 = p;
	    	System.out.println("Paper");
	    		
	    	} else {
	    		player1 = s;
	    		System.out.println("Scissors");
	    	}
	    	if(player2 == s){
	    		player2 = r;
	    	}
	    	else if(player2 == r){
	    		player2 = p;
	    	}
	    	else {
	    		player2 = s;
	    	}
	    	
	    	if(player1 == r){
	    		if(player2 == r){
	    			tie++;
	    		}
	    		else if(player2 == p){
	    			p2Win++;
	    		}
	    		else{
	    			p1Win++;
	    		}
	    		
	    			
	    		}
	    	else if (player1 == p){
	    		if(player2 == r){
	    			p1Win++;
	    		}
	    	
	    	else if (player2 == p){
	    		tie++;
	    	}
	    	else{
	    		p2Win++;
	    		
	    		
	    		}
	    	}
	    		
	    	else
	     	{
    			if(player2 == p)
    			{
    				p1Win++;
    			}
    			
	    	
	    		
    		else if(player2 == r){
    			p2Win++;
	    		}
    		else{
    			tie++;
	    
	    	}
	    
	    
	    	}
	
	    	
	    
	  
	    			
	    		}
	    System.out.println("Player 1 win total " + p1Win + "/" + rounds);
	    System.out.println("Player 2 win total " + p2Win + "/" + rounds);
	    System.out.println("Amount of ties " + tie + "/" + rounds);
	    
	    double oneWinPercentage = p1Win / rounds;
	    double twoWinPercentage = p2Win / rounds;
	    double tiesPercentage = tie / rounds;
	    
	    System.out.println("Player 1 Win Rate " + oneWinPercentage);
	    System.out.println("Player 2 Win Rate " + twoWinPercentage);
	    System.out.println("Tie Rate " + tiesPercentage);
	    
	    
	    
	}
}
